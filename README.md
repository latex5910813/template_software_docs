A LaTeX template for software projects

# Setup

## Makefile

  Change the value for `PROJECT_NAME` to fit your projects name 

# Custom Autocompletion

For a detailed explanation of .cwl files see 
https://htmlpreview.github.io/?https://github.com/texstudio-org/texstudio/master/utilities/manual/usermanual_en.html#description-of-the-cwl-format
