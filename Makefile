##################################
# Makefile by Emanuel Oberholzer #
##################################

# Modify the following line for naming the end product pdf file
PROJECT_NAME=Manus Filia

PDF_VIEWER=evince

# The filename of the coversheet in your coversheet directory.
#
# The coversheet MUST be an ODT file.
COVERSHEET=coversheet.odt

# The defaul target for which the document should be optimized. Before
# using this value, the makefile checks if there is an environment variable
# called OPTIMIZATION_TARGET.
#
# Must be either "pdf" or "book".
#
# After the optimization target has changed you must execute
# purge target.
# DEFAULT_OPTIMIZATION_TARGET=pdf
DEFAULT_OPTIMIZATION_TARGET=book

# Whether or not an interleaf page should be included, after
# the coversheet.
#
# Must be either "true" or "false".
INCLUDE_INTERLEAF_PAGE_AFTER_COVERSHEET=true

# The engine that should be used to convert the TeX document to an ODT
# file. Different engines produce differently optimized results.
#
# Must be one of "lowriter", "pandoc" or "make4ht".
#
# ODT_CONV_ENGINE=lowriter
# ODT_CONV_ENGINE=pandoc
ODT_CONV_ENGINE=make4ht

######################## REQUIRED COMMANDS ########################

MAKE4HT_CMD=make4ht
PDFLATEX_CMD=pdflatex
#BIBTEX_CMD=bibtex
BIBTEX_CMD=biber
LOWRITER_CMD=lowriter
PANDOC_CMD=pandoc
GHOSTSCRIPT_CMD=gs
TEXCOUNT_CMD=texcount

######################## OPTIONAL COMMANDS ########################
# Optonal commands can be left empty or be commented out like this:
#
# #THING_CMD=thing
# THING_CMD=

PDFTK_CMD=pdftk

######################## COMMAND OPTIONS ########################

# debug, info, status, warning, error, fatal
MAKE4HT_LOG_LEVEL=status

# tex4ht or lua4ht
#
# For tex4ht docs see https://www.kodymirus.cz/tex4ht-doc/Introduction.html
# Also see https://tex.stackexchange.com/questions/285912/examples-of-html-documentation-produced-with-latex
MAKE4HT_BACKEND=tex4ht

# See filters https://cgit.freedesktop.org/libreoffice/core/tree/filter/source/config/fragments/filters
LOWRITER_OUT_FILTER=OpenDocument Text Flat XML
LOWRITER_OUT_FILE_EXT=fodt

######################## DO NOT EDIT ########################
# some Makefile-hints taken from: 
# http://www.ctan.org/tex-archive/help/uk-tex-faq/Makefile

# Function to get the value of the environment variable OPTIMIZATION_TARGET,
# or the value of DEFAULT_OPTIMIZATION_TARGET if the env variable does
# not exist. 
GET_OPTIMIZATION_TARGET := \
	if [ -z "${OPTIMIZATION_TARGET}" ]; then \
		echo "${DEFAULT_OPTIMIZATION_TARGET}"; \
	else \
		echo "${OPTIMIZATION_TARGET}"; \
	fi

OPTIMIZATION_TARGET=$(shell ${GET_OPTIMIZATION_TARGET})

MAIN_DOC_WITHOUT_TARGET=main_
MAIN_DOC_EXT=tex

MAIN_DOC_BASENAME=${MAIN_DOC_WITHOUT_TARGET}${OPTIMIZATION_TARGET}
MAIN_DOC_FILENAME=${MAIN_DOC_BASENAME}.${MAIN_DOC_EXT}
TIMESTAMP=$(shell /bin/date +%Y-%m-%d)

PDF_BASENAME=${TIMESTAMP}_${PROJECT_NAME}
ODT_BASENAME=${PDF_BASENAME}

PDF_FILENAME=${PDF_BASENAME}.pdf
COMPRESSED_QUALITY_PDF_FILENAME=compressed-quality_${PDF_FILENAME}
COMPRESSED_SIZE_PDF_FILENAME=compressed-size_${PDF_FILENAME}

CHECK_PDF_EXISTS := \
	if ! [ -f "${PDF_FILENAME}" ]; then \
		echo \
			"The output file '${PDF_FILENAME}' does not exist, " \
			"maybe you have not compiled anything yet ?"; \
		exit 1; \
	fi

HTML_BUILD_DIR=html_build
HTML_OUT_DIR=html_out

# The directory, which contains the config files for
# the specific make4ht output formats.
MAKE4HT_HTML_CONFIG_DIR=assets/make4ht_config

MAKE4HT_HTML_CONFIG_FILE=${MAKE4HT_HTML_CONFIG_DIR}/html.tex

# Uncomment next line, if you want to use the config file for odt.
# MAKE4HT_ODT_CONFIG_FILE=${MAKE4HT_HTML_CONFIG_DIR}/odt.tex

COVERSHEETS_DIR=coversheet
COVERSHEET_PATH=${COVERSHEETS_DIR}/${COVERSHEET}
COVERSHEET_BASENAME=$(shell basename -s .odt ${COVERSHEET})
COVERSHEET_INTERLEAF_PAGE_NAME=interleaf_page

TEX_CONTENT_DIR=content

######################## Targets ########################

#help
#helpThe main targets of this Makefile are:
#help	help			Print this message
.PHONY: help
help:
	@sed -n 's/^#help//p' < Makefile

#help
#help 	cover			Compile the coversheet at to a PDF. Uses the lowriter cli tool
.PHONY: cover
cover:
	@${LOWRITER_CMD} \
		--convert-to pdf:writer_pdf_Export \
		--outdir "${COVERSHEETS_DIR}" \
		"${COVERSHEET_PATH}"
ifeq (${INCLUDE_INTERLEAF_PAGE_AFTER_COVERSHEET},true)
	@${LOWRITER_CMD} \
		--convert-to pdf:writer_pdf_Export \
		--outdir "${COVERSHEETS_DIR}" \
		"${COVERSHEETS_DIR}/${COVERSHEET_INTERLEAF_PAGE_NAME}.odt"

	@${GHOSTSCRIPT_CMD} \
		-dBATCH \
		-dNOPAUSE \
		-dQUIET \
		-sDEVICE=pdfwrite \
		-sOutputFile="${COVERSHEETS_DIR}/${COVERSHEET_BASENAME}_tmp.pdf" \
		"${COVERSHEETS_DIR}/${COVERSHEET_BASENAME}.pdf" \
		"${COVERSHEETS_DIR}/${COVERSHEET_INTERLEAF_PAGE_NAME}.pdf"

	@mv -f \
		"${COVERSHEETS_DIR}/${COVERSHEET_BASENAME}_tmp.pdf" \
		"${COVERSHEETS_DIR}/${COVERSHEET_BASENAME}.pdf"
else ifeq (${INCLUDE_INTERLEAF_PAGE_AFTER_COVERSHEET},false)
else
	@echo \
		"The value of 'INCLUDE_INTERLEAF_PAGE_AFTER_COVERSHEET' must be either 'true' " \
		"or 'false'. The value '${INCLUDE_INTERLEAF_PAGE_AFTER_COVERSHEET}' is invalid"
	@exit 1
endif

#help
#help	pdf			Create a PDF file using pdflatex and bibtex/biber
.PHONY: pdf
pdf:
	${PDFLATEX_CMD} ${MAIN_DOC_FILENAME}
	-${BIBTEX_CMD} ${MAIN_DOC_BASENAME}
	${PDFLATEX_CMD} ${MAIN_DOC_FILENAME}
	${PDFLATEX_CMD} ${MAIN_DOC_FILENAME}
	@cp "${MAIN_DOC_BASENAME}.pdf" "${PDF_FILENAME}"

#help
#help	pdf-quick		Create a PDF file using only pdflatex
.PHONY: pdf-quick
pdf-quick:
	${PDFLATEX_CMD} ${MAIN_DOC_FILENAME}
	@cp "${MAIN_DOC_BASENAME}.pdf" "${PDF_FILENAME}"

#help
#help	combine			Combine the compiled PDF from the TeX source with the compiled PDF from the coversheet.
.PHONY: combine
combine: cover
	@${CHECK_PDF_EXISTS}
	@echo
	@./scripts/combine.sh \
		"${GHOSTSCRIPT_CMD}" \
		"${PDFTK_CMD}" \
		"${COVERSHEETS_DIR}/${COVERSHEET_BASENAME}.pdf" \
		"${PDF_FILENAME}" \

#help
#help	compress-size		Compress the compiled PDF. This reduces the filesize by a lot by reducing the image quality. 
.PHONY: compress-size
compress-size:
	@${CHECK_PDF_EXISTS}
	@./scripts/compress.sh \
		"${GHOSTSCRIPT_CMD}" \
		size \
		"${PDF_FILENAME}" \
		"${COMPRESSED_SIZE_PDF_FILENAME}" \
		"${COMPRESSED_QUALITY_PDF_FILENAME}"

#help
#help	compress-quality	Compress the compiled PDF. This reduces the filesize while retaining high image quality.
#help				For maximum size compression use 'compress-size'.
.PHONY: compress-quality
compress-quality:
	@${CHECK_PDF_EXISTS}
	@./scripts/compress.sh \
		"${GHOSTSCRIPT_CMD}" \
		quality \
		"${PDF_FILENAME}" \
		"${COMPRESSED_SIZE_PDF_FILENAME}" \
		"${COMPRESSED_QUALITY_PDF_FILENAME}"

#help
#help	all			Compile the coversheet and the TeX source to a PDF and then combine them.
#help				This used the "pdf" target
.PHONY: all
all: cover pdf combine

#help
#help	all-quick		Compile the coversheet and the TeX source to a PDF and then combine them.
#help				This used the "pdf-quick" target
.PHONY: all-quick
all-quick: cover pdf-quick combine

#help
#help	view			Compile the entire project and view it in the ${PDF_VIEWER}
#help				This used the "all" target
.PHONY: view
view: all .internal.view

#help
#help	view-quick		Compile the entire project and view it in the ${PDF_VIEWER}
#help				This used the "all-quick" target
.PHONY: view-quick
view-quick: all-quick .internal.view

#help
#help	wc			Count the words from the TeX document
.PHONY: wc
wc:
	@./scripts/wordcount.sh \
		"${MAIN_DOC_FILENAME}" \
		"${TEXCOUNT_CMD}"

#help
#help	clean			Remove all temporary files
.PHONY: clean
clean:
	@latexmk -C > /dev/null 2>&1 || true
	@find . -type f -name "*.4og" -delete
	@rm -rf *.ps \
					"${PDF_BASENAME}.txt" \
					*.bbl \
					*.ps \
					*.lod \
					*.bbl-SAVE-ERROR \
					*.synctex\(busy\) \
					"${HTML_BUILD_DIR}" \
					*.4ct \
					*.4tc \
					*.4of \
					*.4ot \
					*.4os \
					*.4oy \
					*.4od \
					*.4oo \
					*.idv \
					*.lg \
					*.tmp \
					*.xref \

#help
#help	purge			Remove all temporary and compiled files
.PHONY: purge
purge: clean
	@rm -rf "${COVERSHEETS_DIR}"/*.pdf
	@rm -rf *.pdf \
					*.odt \
					"${HTML_OUT_DIR}" \

#help
#help	install_autocompletion	Install autocompletion for custom commands
.PHONY: install_autocompletion
install_autocompletion:
	@./scripts/install_autocompletion.sh

#help
#help	html			Compile the project to a HTML webpage.
.PHONY: html
html: 
	@echo "Converting project to a HTML webpage..."
	@mkdir -p "${HTML_BUILD_DIR}/assets/images"
	@cp -r assets/images/* "${HTML_BUILD_DIR}/assets/images"
	@${MAKE4HT_CMD} \
		--backend ${MAKE4HT_BACKEND} \
		--loglevel ${MAKE4HT_LOG_LEVEL} \
		--build-dir "${HTML_BUILD_DIR}" \
		--output-dir "${HTML_OUT_DIR}" \
		--config "${MAKE4HT_HTML_CONFIG_FILE}" \
		"${MAIN_DOC_FILENAME}" || true

#help
#help	odt			Compile the project to an OpenDocument Text file.
# See https://askubuntu.com/a/239332 
.PHONY: odt
odt:
ifeq (${ODT_CONV_ENGINE},lowriter)
	@$(MAKE) .internal.odt.lowriter
else ifeq (${ODT_CONV_ENGINE},pandoc)
	@$(MAKE) .internal.odt.pandoc
else ifeq (${ODT_CONV_ENGINE},make4ht)
	@$(MAKE) .internal.odt.make4ht
else
	@echo "The selected html conversion engine '${ODT_CONV_ENGINE}' is not supported"
	@exit 1
endif

#help
#help	release			Compile the project for all optimization targets (book, pdf) and put the
#help				result into a subdirectory in the 'releases' directory.
.PHONY: release
release:
	@./scripts/release.sh \
		"${PDF_FILENAME}" \
		"${COMPRESSED_SIZE_PDF_FILENAME}" \
		"${COMPRESSED_QUALITY_PDF_FILENAME}"

#help
#help	search			Search through the content of all your tex files to find a certain string.
.PHONY: search
search:
	@read -r -p "What do you want to search for: " query_str; \
		echo ; \
		grep \
			--color \
			--with-filename \
			--line-number \
			"$$query_str" \
			$(shell find "${TEX_CONTENT_DIR}" -type f -name "*.tex") || \
	 echo "The text '$$query_str' does not occur in any .tex " \
	 			"file in the directory '${TEX_CONTENT_DIR}'"

######################## internal targets ########################

.PHONY: .internal.view
.internal.view: 
	@${CHECK_PDF_EXISTS}
	@${PDF_VIEWER} "${PDF_FILENAME}"

#
.PHONY: .internal.odt.lowriter
.internal.odt.lowriter: html
	@./scripts/lowriter_html_to_odt.sh \
		"${HTML_OUT_DIR}/${MAIN_DOC_BASENAME}.html" \
		"${COVERSHEETS_DIR}/${COVERSHEET}" \
		"${ODT_BASENAME}" \
		"${LOWRITER_CMD}" \
		"${PANDOC_CMD}" \
		"${LOWRITER_OUT_FILTER}"

#
.PHONY: .internal.odt.pandoc
.internal.odt.pandoc: html
	@./scripts/pandoc_html_to_odt.sh \
		"${HTML_OUT_DIR}/${MAIN_DOC_BASENAME}.html" \
		"${COVERSHEETS_DIR}/${COVERSHEET}" \
		"${ODT_BASENAME}" \
		"${LOWRITER_CMD}" \
		"${PANDOC_CMD}"

#
.PHONY: .internal.odt.make4ht
.internal.odt.make4ht: 
ifdef MAKE4HT_ODT_CONFIG_FILE
	@${MAKE4HT_CMD} \
		--loglevel ${MAKE4HT_LOG_LEVEL} \
		--config ${MAKE4HT_ODT_CONFIG_FILE} \
		-f odt \
		"${MAIN_DOC_WITHOUT_TARGET}odt.${MAIN_DOC_EXT}" || true
else
	@${MAKE4HT_CMD} \
		--loglevel ${MAKE4HT_LOG_LEVEL} \
		-f odt \
		"${MAIN_DOC_WITHOUT_TARGET}odt.${MAIN_DOC_EXT}" || true
endif

	@# Only move file if it exists
	@test -f "${MAIN_DOC_WITHOUT_TARGET}odt.odt" && \
		mv -f "${MAIN_DOC_WITHOUT_TARGET}odt.odt" "${PDF_BASENAME}.odt"
