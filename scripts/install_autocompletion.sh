#! /bin/bash

# Copyright (C) 2022 Emanuel Oberholzer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# shellcheck disable=SC2155

######################## Constants ########################

readonly REPORT_EMAIL="emanueloberholzer400@gmail.com"
readonly AUTHOR="Emanuel Oberholzer"

readonly PROGRAM_NAME="install_autocompletion"

readonly AUTOCOMPLETION_DIRPATH="$(pwd)/assets/autocompletion"

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_BOLD="\x1B[1;37m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_DEBUG="\x1B[38;5;39m"
readonly TEXT_STYLE_FG_VERBOSE=$TEXT_STYLE_BOLD

# Used in function log()
logLevel=1

######################## Functions ########################

# Print a log message to stdout.
#
# PARAMETERS
#   1. The log level of the message(=$1)
#
# If the message's log level is error
#   2. Exit code for the script(=$2)
#   3. The message that should be printed(=$3)
#   4. Extra code to evaluate before exiting(=$4)
#
# If the message's log level is NOT error
#   2. The message that should be printed(=$2)
#
# LOG LEVELS
# verbose -> 1
# debug -> 2
# info -> 3
# warning -> 4
# error -> 5
#
# EXAMPLE
# log 3 "My info message"
# log 5 24 "My error message with exit code 24"
function log() {
  if [ "$logLevel" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_VERBOSE}VERBOSE:${TEXT_STYLE_END} $*"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_DEBUG}DEBUG:${TEXT_STYLE_END} $*"
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_INFO}INFO:${TEXT_STYLE_END} $*"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_WARNING}WARNING:${TEXT_STYLE_END} $*"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    echo -e "${TEXT_STYLE_FG_ERROR}ERROR!${TEXT_STYLE_END}"
    echo -e "$3"
    echo

    eval "$4"

    exit "$2"
  fi
}

# Print info at the start of the script.
function info() {
  echo "$PROGRAM_NAME v$VERSION"
  echo "Author: $AUTHOR"
  echo "Report Email: $REPORT_EMAIL"
  echo
}

function help() {
  echo "This script copies the autocompletion files from the directory "
  echo "'$AUTOCOMPLETION_DIRPATH' into the users configuration directory "
  echo "for the specified IDE, in order to enable the autocompletion feature "
  echo "for the custom functions in the project."
  echo
  echo "You can also uninstall the file from the users configuration "
  echo "directory."

  exit 0
}

# PARAMETERS
#   1. install (true) or uninstall (false)
#   2. Path to the directory that contains the autocompletion files
function installTeXstudio() {
  local userDir="$HOME/.config/texstudio/completion/user"
  local install=$1
  local projectAutocompletionDir="$2"

  # Check if userDir exists.
  if ! [ -d "$userDir" ]; then
    # Cannot uninstall something that does not exist
    if [ "false" = "$install" ]; then
      log 3 "The directory '$userDir' does not exist, therefore there is nothing that can be uninstalled."
      return
    fi

    log 5 101 "The directory '$userDir' does not exist"
  fi

  if ! [ -d "$projectAutocompletionDir" ]; then
    log 5 102 "The directory '$projectAutocompletionDir' does not exist"
  fi

  # Install files from the projects autocompletion dir.
  if [ "true" = "$install" ]; then
    # Iterate over every *.cwl file in $projectAutocompletionDir .
    find "$projectAutocompletionDir" -type f -iname "*.cwl" | while read -r file; do
      cp "$file" "$userDir"
      local status=$?

      if [ 0 -eq $status ]; then
        log 3 "Successfully copied ${TEXT_STYLE_BOLD}'$file'${TEXT_STYLE_END} to ${TEXT_STYLE_BOLD}'$userDir'${TEXT_STYLE_END}."
      else
        log 5 103 "Failed to copy '$file' to '$userDir'."
      fi
    done

    echo
    log 3 \
      "Now open TeXstudio and navigate to ${TEXT_STYLE_BOLD}Options -> Configure TeXstudio${TEXT_STYLE_END}" \
      "and enable the ${TEXT_STYLE_BOLD}Show Advanced Options${TEXT_STYLE_END} button in the" \
      "bottom left corner. Afterwards navigate to the submenu ${TEXT_STYLE_BOLD}Completion${TEXT_STYLE_END}" \
      "and search for the installed files and active them."

  else # Uninstall files from the projects autocompletion dir.
    # Iterate over every *.cwl file in $projectAutocompletionDir .
    find "$projectAutocompletionDir" -type f -iname "*.cwl" | while read -r file; do
      local installedFile="$userDir/$(basename "$file")"

      if ! [ -e "$installedFile" ]; then
        log 1 "The file '$installedFile' does not exist, therefore there is nothing that can be uninstalled."
        continue
      fi

      rm -f "$installedFile"
      log 3 "Uninstalled ${TEXT_STYLE_BOLD}'$installedFile'${TEXT_STYLE_END}."
    done
  fi
}

######################## Script ########################

info

echo "Choose what you want to do. Default is install."
echo "1) Install"
echo "2) Uninstall"
echo "3) Help"
read -r chosenMode

case $chosenMode in
1) shouldInstall="true" ;;
2) shouldInstall="false" ;;
3) help ;;
*) shouldInstall="true" ;;
esac
echo

# Get the path to the project directory.
projectDir=$(pwd)
autocompletionDirpath="$projectDir/assets/autocompletion"
if ! [ -d "$autocompletionDirpath" ]; then
  log 5 1 "The directory '$autocompletionDirpath' that should contain the autocompletion files does not exist."
fi

## Find out the useres ide.
echo "Choose your LaTeX IDE."
echo "1) TeXstudio"
read -r chosenIde

case $chosenIde in
1)
  echo
  installTeXstudio $shouldInstall "$autocompletionDirpath"
  ;;

*) help ;;
esac

exit 0
