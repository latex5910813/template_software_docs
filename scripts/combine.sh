#! /bin/sh

######################## Script ########################

ghostscript_exe=$1
pdftk_exe=$2
coversheet_filename=$3
pdf_filename=$4

echo "Adding coversheet to pdf..."

outfile="$(mktemp)"

if [ -n "$pdftk_exe" ]; then
  "$pdftk_exe" \
    "$coversheet_filename" \
    "$pdf_filename" \
    cat \
    output \
    "$outfile"
elif [ -n "$ghostscript_exe" ]; then
  echo \
    "Using GhostScript to add the coversheet, because PDFTK" \
    "is not available. Keep in mind that when using GhostScript to" \
    "add the coversheet there will always be some level of compression." \
    "If you do not want to have any compression, you must use PDFTK."

  # Set PDFSETTINGS to /default to retain highest quality
  # See https://ghostscript.readthedocs.io/en/latest/VectorDevices.html#controls-and-features-specific-to-postscript-and-pdf-input
  "$ghostscript_exe" \
    -dBATCH \
    -dNOPAUSE \
    -dQUIET \
    -dPDFSETTINGS=/default \
    -sDEVICE=pdfwrite \
    -sOutputFile="$outfile" \
    "$coversheet_filename" \
    "$pdf_filename"
else
  echo "Neither pdftk nor ghostscript is available"
  exit 1
fi

mv -f "$outfile" "$pdf_filename"
echo "Finished !"
