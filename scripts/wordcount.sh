#! /bin/bash

######################## Script ########################

main_tex_filename=$1
texcount_exe=$2

# 2492+46+177 (30/31/0/0) File: main_pdf.tex
counts="$($texcount_exe -merge -incbib -quiet -brief -utf8 "$main_tex_filename" 2>/dev/null | grep "$main_tex_filename")"

# text+headers+captions (#headers/#floats/#inlines/#displayed)
mapfile -d "+" -t word_counts < <(echo "$counts" | cut -d" " -f1 | tr -d "\n")
mapfile -d "/" -t special_counts < <(echo "$counts" | cut -d" " -f2 | tr -d "(-)" | tr -d "\n")

((total_words = word_counts[0] + word_counts[1] + word_counts[2]))

echo "Words in text: ${word_counts[0]}"
echo "Words in headers: ${word_counts[1]}"
echo "Words outside text (captions, etc.): ${word_counts[2]}"
echo "Total words: $total_words"
echo
echo "Number of headers: ${special_counts[0]}"
echo "Number of floats/tables/figures: ${special_counts[1]}"
echo "Number of math inlines: ${special_counts[2]}"
echo "Number of math displayed: ${special_counts[3]}"
