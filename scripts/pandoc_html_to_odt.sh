#! /bin/bash

# Convert a html file to a libreoffice odt file
# Copyright (C) 2024 Emanuel Oberholzer

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

######################## Constants ########################

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_BOLD="\x1B[1;37m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_DEBUG="\x1B[38;5;39m"
readonly TEXT_STYLE_FG_VERBOSE=$TEXT_STYLE_BOLD

# Used in function log()
logLevel=1

######################## Functions ########################

# Print a log message to stdout.
#
# PARAMETERS
#   1. The log level of the message(=$1)
#
# If the message's log level is error
#   2. Exit code for the script(=$2)
#   3. The message that should be printed(=$3)
#   4. Extra code to evaluate before exiting(=$4)
#
# If the message's log level is NOT error
#   2. The message that should be printed(=$2)
#
# LOG LEVELS
# verbose -> 1
# debug -> 2
# info -> 3
# warning -> 4
# error -> 5
#
# EXAMPLE
# log 3 "My info message"
# log 5 24 "My error message with exit code 24"
function log() {
  if [ "$logLevel" -gt "$1" ]; then
    return
  fi

  # verbose
  if [ "$1" -eq 1 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_VERBOSE}VERBOSE:${TEXT_STYLE_END} $*"
    return
  fi

  # debug
  if [ "$1" -eq 2 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_DEBUG}DEBUG:${TEXT_STYLE_END} $*"
    return
  fi

  # info
  if [ "$1" -eq 3 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_INFO}INFO:${TEXT_STYLE_END} $*"
    return
  fi

  # warning
  if [ "$1" -eq 4 ]; then
    shift
    echo -e "${TEXT_STYLE_FG_WARNING}WARNING:${TEXT_STYLE_END} $*"
    return
  fi

  # error
  if [ "$1" -eq 5 ]; then
    echo -e "${TEXT_STYLE_FG_ERROR}ERROR!${TEXT_STYLE_END}"
    echo -e "$3"
    echo

    eval "$4"

    exit "$2"
  fi
}

######################## Script ########################

project_dir="$(pwd)"

html_idx_filepath="$(readlink -fn "$project_dir/$1")"
coversheet_filepath="$(readlink -fn "$project_dir/$2")"
outfile_basename=$3
lowriter_exe=$4
pandoc_exe=$5

if ! [ -f "$html_idx_filepath" ]; then
  log 5 1 "The html index file at '$html_idx_filepath' does not exist"
fi

if ! [ -f "$coversheet_filepath" ]; then
  log 5 2 "The ODT coversheet file at '$coversheet_filepath' does not exist"
fi

tmp_dirpath=$(mktemp -d)
outfile="$project_dir/$outfile_basename.odt"
coversheet_basename="$(basename -s .odt "$coversheet_filepath")"

echo "Converting project to an OpenDocument Text file using pandoc..."

# Convert coversheet to HTML
if ! $lowriter_exe --headless --convert-to html "$coversheet_filepath" --outdir "$tmp_dirpath"; then
  log 5 3 "Conversion failed"
fi

# TODO(obemu): Fix conversion of coversheet
# $pandoc_exe -t odt -o "$outfile" "$tmp_dirpath/$coversheet_basename.html" "${html_idx_filepath}"
$pandoc_exe -t odt -o "$outfile" "${html_idx_filepath}"

echo "Successfully created '$outfile'"
