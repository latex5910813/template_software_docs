#! /bin/sh

######################## Script ########################

project_dir="$(pwd)"

pdf_filename=$1
compressed_size_pdf_filename=$2
compressed_quality_pdf_filename=$3

outdir="$project_dir/releases/$(date +%Y-%m-%d_%H-%M-%S)"
mkdir -p "$outdir"

exec_make() {
  make purge
  
  # OPTIMIZATION_TARGET is used by make all
  export OPTIMIZATION_TARGET="$1"
  make all

  # Copy original file
  cp "$pdf_filename" "$outdir/optimized-${1}_${pdf_filename}"

  # Compress original file and move the result into the outdir
  make compress-size
  mv "$compressed_size_pdf_filename" "$outdir/optimized-${1}_${compressed_size_pdf_filename}"
}

exec_make book
exec_make pdf
date > "$outdir/timestamp.txt"

echo
echo
echo
echo "Created release at '$outdir'"
echo

exit 0
