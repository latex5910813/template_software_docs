#! /bin/sh

######################## Functions ########################

get_filesize_in_bytes() {
  find "$(pwd)" -name "$1" -printf "%s"
}

to_human_readable_filesize() {
  size=$1
  unit_idx=$((0))

  while [ "$size" -gt 1024 ]; do
    size=$((size / 1024))
    unit_idx=$((unit_idx + 1))
  done

  case $unit_idx in
  0) unit="B" ;;
  1) unit="KB" ;;
  2) unit="MB" ;;
  3) unit="GB" ;;
  4) unit="TB" ;;
  5) unit="PB" ;;
  6) unit="EB" ;;
  7) unit="ZB" ;;
  8) unit="YB" ;;
  *)
    echo "Unsupported filesize"
    exit 1
    ;;
  esac

  echo "$size $unit"
}

######################## Script ########################

ghostscript_exe=$1
compression_target=$2
pdf_filename=$3
compressed_size_pdf_filename=$4
compressed_quality_pdf_filename=$5

if [ "size" = "$compression_target" ]; then
  echo "Creating a pdf with high image compression..."
  "$ghostscript_exe" \
    -sDEVICE=pdfwrite \
    -dCompatibilityLevel=1.4 \
    -dPDFSETTINGS=/ebook \
    -dNOPAUSE \
    -dQUIET \
    -dBATCH \
    -sOutputFile="$compressed_size_pdf_filename" \
    "$pdf_filename"
  echo "Successfully created '$compressed_size_pdf_filename' !"
  result_filename=$compressed_size_pdf_filename
elif [ "quality" = "$compression_target" ]; then
  echo "Creating a pdf with moderate compression while retaining high image quality..."
  "$ghostscript_exe" \
    -sDEVICE=pdfwrite \
    -dCompatibilityLevel=1.4 \
    -dPDFSETTINGS=/default \
    -dNOPAUSE \
    -dQUIET \
    -dBATCH \
    -sOutputFile="$compressed_quality_pdf_filename" \
    "$pdf_filename"
  echo "Successfully created '$compressed_quality_pdf_filename' !"
  result_filename=$compressed_quality_pdf_filename
else
  echo "The compression target '$compression_target' is not supported"
  exit 1
fi

pdf_filesize=$(get_filesize_in_bytes "$pdf_filename")
result_filesize=$(get_filesize_in_bytes "$result_filename")
filesize_diff=$((pdf_filesize - result_filesize))

pdf_filesize=$(to_human_readable_filesize "$pdf_filesize")
result_filesize=$(to_human_readable_filesize "$result_filesize")
filesize_diff=$(to_human_readable_filesize "$filesize_diff")

echo
echo "The original file ($pdf_filename) has a size of $pdf_filesize"
echo "The compressed file ($result_filename) has a size of $result_filesize"
echo "This is a difference of $filesize_diff"
