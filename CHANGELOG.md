# 1.1.6

  * Added commands `\refNameParagraph`, `\refShortParagraph`, `\refSeeShortParagraph` and `\refSeeShortParagraphBr` to `config/ref_commands.tex` and `assets/autocompletion/template_software_docu_ref_commands.cwl`

  * Added commands `\bookInterleafPage`, `\bookOpenLeft` and `\bookOpenRight` to `config/preamble.tex` and `assets/autocompletion/template_software_docu_preamble.cwl`

  * Added custom caption format `myformat` in `config/typographic_settings.tex`

  * Added configuration for entries in the generated list of figures in `config/figures.tex`

# 1.1.5

  * This time really fixed the implementation of `\myRefGetNameWithoutLinebreaks`

# 1.1.4

  * Removed command `\removeLinebreaks`

  * Fixed implementation of `myRefGetNameWithoutLinebreaks`

  * Added `config/overrides.tex`

# 1.1.3

  * Renamed `\@myRefOverrideNameref` to `\myRefGetNameWithoutLinebreaks`

  * Removed `\myRefCommandRemoveLinebreaks`

  * Added config option `\myConfigCaptionJustification`

# 1.1.2

  * Added config option `\myConfigCaptionWidth`

  * Added command `\removeLinebreaks` to `config/utility_commands.tex` and `assets/autocompletion/template_software_docu_utility_commands.cwl`

  * Added internal command `\@myRefOverrideNameref` to `config/ref_commands.tex` and `assets/autocompletion/template_software_docu_ref_commands.cwl`

# 1.1.1

  * Added make target `search`

# 1.1.0

  * Improved readability for `config/doc_class_book.tex` and `config/doc_class_pdf.tex`

  * Added package `caption` to format figure captions better

  * Added comments to the font definitions in `config/typographic_settings.tex` so that it is clear which parts of the document are affected by the specific font.

  * Added config options `\myConfigShowChapterPrefix` and `\myConfigDocumentHeadings`

  * Added package `cleverref`

  * Added the new commands `\refFullParagraph`, `\refSeeParagraph` and `\refSeeParagraphBr` to `config/ref_commands.tex` and `assets/autocompletion/template_software_docu_ref_commands.cwl`

  * Added chapter `content/ch-references.tex` which showcases the new commands from `config/ref_commands.tex`

  * Renamed command `refSeeName` to `refNameSeeShortBr`

# 1.0.17

  * Added option to use ghostscript or pdftk to combine the coversheet with the compiled pdf

  * Added option to compression targets for size or for quality. 

# 1.0.16

  * Bugfix in `main.tex`. Needed to put `\addcontentsline{toc}{chapter}{\contentsname}` after `\tableofcontents`, otherwise the clickable hyperlink is messed up and leads to the empty page when compiling for a book.

# 1.0.15

  * Bugfix in new command `\refFull` from `config/ref_commands.tex`. Now the hyphen character is a clickable link as well

# 1.0.14

  * Renamed `\myConfigBookEmptyPageStyle` to `\myConfigBookInterleafPageStyle`

  * Added support to use `make4ht` as a conversion engine to ODT

  * Code cleanup in makefile

# 1.0.13

  * Added setting `\myConfigBookEmptyPageStyle` to `main.tex`

# 1.0.12

  * Added setting `\myConfigSectionNumberingDepth` to `main.tex`. This allows the user to configure the section and ToC depth.

  * Added setting `\myConfigShowRulers` which splits the old setting `\myConfigUseDraftMode` into two.

  * Improved makefile

# 1.0.11

  * Removed usages of `MAKEIDX_CMD` and `PDFTK_CMD` in makefile

  * Improved html conversion

# 1.0.10

  * Added option `INCLUDE_INTERLEAF_PAGE_AFTER_COVERSHEET` to `Makefile`. This option allows the user to include a page after the coversheet.

  * Added total wordcount to make target `wc`

  * Added `\clearpage` commands to chapter `2 Frontend`, to fix the formatting before the page breaks

  * Added make target `release`. This target compiles both optimized versions, the one for `pdf` and the one for `book` and puts them into a seperate directory. The target also creates the compressed versions.

  * improved .gitignore file

# 1.0.9

  * Added frontmatter for `content/gender.tex`

# 1.0.8

  * Addeded `./scripts/wordcount.sh` which is a script that counts the words in the TeX source. This script is now used in the make target `wc`

# 1.0.7

  * Bugfix in commands `enquoteIt` and `enquoteBf`

# 1.0.6

  * Added commands `enquoteIt` and `enquoteBf` to `./config/utility_commands.tex` and `./assets/autocompletion/template_software_docu_utility_commands.cwl`

# 1.0.5

  * Improved \ref* commands from `./config/ref_commands.tex`. Now the custom word for `see (deutsch: siehe)` is a clickable part of the hyperlink as well

# 1.0.4

  * Added `compress` target to Makefile

  * Added checks to targets `view-quick`, `view` and `combine` to ensure that the output pdf file from the LaTeX source code exists before the actual commands of the target are executed. This helps the user with identifying the error, when a target fails.

# 1.0.3

  * Added rule `*.tdo` to .gitignore

# 1.0.2

  * Added `html` target to `odt` target in Makefile

# 1.0.1

  * Changed the implementation of the `\addTodosChapter` command

# 1.0.0

  * Added boolean flag `\myConfigOptimizeForPrintableBook` to change the document class to `scrbook` or `scrreprt`

  * Added `main_book.tex` and `main_pdf.tex` to set `\myConfigOptimizeForPrintableBook` because it should not be manually modified by the user

  * Added variable `OPTIMIZATION_TARGET` to `Makefile`

# 0.0.1

  * Initial release
