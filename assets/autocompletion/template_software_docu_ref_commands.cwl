# Autocompletion for commands from config/ref_commands.tex 
#
# For a detailed explanation of .cwl files see 
# https://htmlpreview.github.io/?https://github.com/texstudio-org/texstudio/master/utilities/manual/usermanual_en.html#description-of-the-cwl-format


# internal commands
\myRefGetNameWithoutLinebreaks{label%ref}

# \ref* commands for labels from \label
\refCustomName{name%text}{label%ref}

\refName{label%ref}
\refNameSeeShortBr{label%ref}
\refNameParagraph{label%ref}

\refFull{label%ref}
\refFullParagraph{label%ref}

\refSee{label%ref}
\refSeeBr{label%ref}
\refSeeParagraph{label%ref}
\refSeeParagraphBr{label%ref}

\refShort{label%ref}
\refShortParagraph{label%ref}

\refSeeShort{label%ref}
\refSeeShortBr{label%ref}
\refSeeShortParagraph{label%ref}
\refSeeShortParagraphBr{label%ref}

# \ref* commands for book biblatex sources
\refBookPar{name%text}{bibid}
\refSeeBookPar{name%text}{bibid}
\refSeeBookParBr{name%text}{bibid}

# \ref* commands for citations
\refLongCite{bibid}
\refSeeLongCite{bibid}
\refSeeLongCiteBr{bibid}
